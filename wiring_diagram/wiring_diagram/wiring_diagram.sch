EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L arduino:Arduino_Due_Shield XA?
U 1 1 60E2E457
P 5100 3650
F 0 "XA?" H 5100 1269 60  0000 C CNN
F 1 "Arduino_Due_Shield" H 5100 1163 60  0000 C CNN
F 2 "" H 5800 6400 60  0001 C CNN
F 3 "https://store.arduino.cc/arduino-due" H 5800 6400 60  0001 C CNN
	1    5100 3650
	1    0    0    -1  
$EndComp
$Comp
L Driver_Motor:L298HN U?
U 1 1 60E305AA
P 8450 2550
F 0 "U?" H 8200 3350 50  0000 C CNN
F 1 "L298HN" H 8200 3250 50  0000 C CNN
F 2 "Package_TO_SOT_THT:TO-220-15_P2.54x2.54mm_StaggerOdd_Lead4.58mm_Vertical" H 8500 1900 50  0001 L CNN
F 3 "http://www.st.com/st-web-ui/static/active/en/resource/technical/document/datasheet/CD00000240.pdf" H 8600 2800 50  0001 C CNN
	1    8450 2550
	1    0    0    -1  
$EndComp
$Comp
L Motor:Motor_Servo M?
U 1 1 60E3168B
P 9750 2450
F 0 "M?" H 10082 2515 50  0000 L CNN
F 1 "Motor_Servo" H 10082 2424 50  0000 L CNN
F 2 "" H 9750 2260 50  0001 C CNN
F 3 "http://forums.parallax.com/uploads/attachments/46831/74481.png" H 9750 2260 50  0001 C CNN
	1    9750 2450
	1    0    0    -1  
$EndComp
Wire Wire Line
	9050 2350 9450 2350
Wire Wire Line
	9050 2450 9450 2450
Wire Wire Line
	9050 2650 9250 2650
Wire Wire Line
	9250 2650 9250 2550
Wire Wire Line
	9250 2550 9450 2550
$Comp
L Driver_Motor:L298HN U?
U 1 1 60E3542E
P 8450 5050
F 0 "U?" H 8200 5850 50  0000 C CNN
F 1 "L298HN" H 8200 5750 50  0000 C CNN
F 2 "Package_TO_SOT_THT:TO-220-15_P2.54x2.54mm_StaggerOdd_Lead4.58mm_Vertical" H 8500 4400 50  0001 L CNN
F 3 "http://www.st.com/st-web-ui/static/active/en/resource/technical/document/datasheet/CD00000240.pdf" H 8600 5300 50  0001 C CNN
	1    8450 5050
	1    0    0    -1  
$EndComp
$Comp
L Motor:Motor_Servo M?
U 1 1 60E35434
P 9750 4950
F 0 "M?" H 10082 5015 50  0000 L CNN
F 1 "Motor_Servo" H 10082 4924 50  0000 L CNN
F 2 "" H 9750 4760 50  0001 C CNN
F 3 "http://forums.parallax.com/uploads/attachments/46831/74481.png" H 9750 4760 50  0001 C CNN
	1    9750 4950
	1    0    0    -1  
$EndComp
Wire Wire Line
	9050 4850 9450 4850
Wire Wire Line
	9050 4950 9450 4950
Wire Wire Line
	9050 5150 9250 5150
Wire Wire Line
	9250 5150 9250 5050
Wire Wire Line
	9250 5050 9450 5050
$Comp
L power:GND #PWR?
U 1 1 60E359BF
P 3800 4900
F 0 "#PWR?" H 3800 4650 50  0001 C CNN
F 1 "GND" V 3805 4772 50  0000 R CNN
F 2 "" H 3800 4900 50  0001 C CNN
F 3 "" H 3800 4900 50  0001 C CNN
	1    3800 4900
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 60E370A0
P 3800 5000
F 0 "#PWR?" H 3800 4750 50  0001 C CNN
F 1 "GND" V 3805 4872 50  0000 R CNN
F 2 "" H 3800 5000 50  0001 C CNN
F 3 "" H 3800 5000 50  0001 C CNN
	1    3800 5000
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 60E37419
P 3800 5100
F 0 "#PWR?" H 3800 4850 50  0001 C CNN
F 1 "GND" V 3805 4972 50  0000 R CNN
F 2 "" H 3800 5100 50  0001 C CNN
F 3 "" H 3800 5100 50  0001 C CNN
	1    3800 5100
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 60E376C5
P 3800 5200
F 0 "#PWR?" H 3800 4950 50  0001 C CNN
F 1 "GND" V 3805 5072 50  0000 R CNN
F 2 "" H 3800 5200 50  0001 C CNN
F 3 "" H 3800 5200 50  0001 C CNN
	1    3800 5200
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 60E378D7
P 3800 5300
F 0 "#PWR?" H 3800 5050 50  0001 C CNN
F 1 "GND" V 3805 5172 50  0000 R CNN
F 2 "" H 3800 5300 50  0001 C CNN
F 3 "" H 3800 5300 50  0001 C CNN
	1    3800 5300
	0    1    1    0   
$EndComp
$Comp
L power:+5V #PWR?
U 1 1 60E37D73
P 3800 5500
F 0 "#PWR?" H 3800 5350 50  0001 C CNN
F 1 "+5V" V 3815 5628 50  0000 L CNN
F 2 "" H 3800 5500 50  0001 C CNN
F 3 "" H 3800 5500 50  0001 C CNN
	1    3800 5500
	0    -1   -1   0   
$EndComp
$Comp
L power:+5V #PWR?
U 1 1 60E38C6B
P 3800 5600
F 0 "#PWR?" H 3800 5450 50  0001 C CNN
F 1 "+5V" V 3815 5728 50  0000 L CNN
F 2 "" H 3800 5600 50  0001 C CNN
F 3 "" H 3800 5600 50  0001 C CNN
	1    3800 5600
	0    -1   -1   0   
$EndComp
$Comp
L power:+5V #PWR?
U 1 1 60E38FDD
P 3800 5700
F 0 "#PWR?" H 3800 5550 50  0001 C CNN
F 1 "+5V" V 3815 5828 50  0000 L CNN
F 2 "" H 3800 5700 50  0001 C CNN
F 3 "" H 3800 5700 50  0001 C CNN
	1    3800 5700
	0    -1   -1   0   
$EndComp
$Comp
L power:+5V #PWR?
U 1 1 60E3955C
P 8550 1850
F 0 "#PWR?" H 8550 1700 50  0001 C CNN
F 1 "+5V" V 8565 1978 50  0000 L CNN
F 2 "" H 8550 1850 50  0001 C CNN
F 3 "" H 8550 1850 50  0001 C CNN
	1    8550 1850
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR?
U 1 1 60E3A207
P 8550 4350
F 0 "#PWR?" H 8550 4200 50  0001 C CNN
F 1 "+5V" V 8565 4478 50  0000 L CNN
F 2 "" H 8550 4350 50  0001 C CNN
F 3 "" H 8550 4350 50  0001 C CNN
	1    8550 4350
	1    0    0    -1  
$EndComp
NoConn ~ 9050 2750
NoConn ~ 9050 5250
$Comp
L power:GND #PWR?
U 1 1 60E3BBD4
P 8450 3250
F 0 "#PWR?" H 8450 3000 50  0001 C CNN
F 1 "GND" H 8455 3077 50  0000 C CNN
F 2 "" H 8450 3250 50  0001 C CNN
F 3 "" H 8450 3250 50  0001 C CNN
	1    8450 3250
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 60E3C3BF
P 8450 5750
F 0 "#PWR?" H 8450 5500 50  0001 C CNN
F 1 "GND" H 8455 5577 50  0000 C CNN
F 2 "" H 8450 5750 50  0001 C CNN
F 3 "" H 8450 5750 50  0001 C CNN
	1    8450 5750
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR?
U 1 1 60E3CAFA
P 7850 2250
F 0 "#PWR?" H 7850 2100 50  0001 C CNN
F 1 "+5V" V 7865 2378 50  0000 L CNN
F 2 "" H 7850 2250 50  0001 C CNN
F 3 "" H 7850 2250 50  0001 C CNN
	1    7850 2250
	0    -1   -1   0   
$EndComp
$Comp
L power:+5V #PWR?
U 1 1 60E3D85E
P 7850 2650
F 0 "#PWR?" H 7850 2500 50  0001 C CNN
F 1 "+5V" V 7865 2778 50  0000 L CNN
F 2 "" H 7850 2650 50  0001 C CNN
F 3 "" H 7850 2650 50  0001 C CNN
	1    7850 2650
	0    -1   -1   0   
$EndComp
$Comp
L power:+5V #PWR?
U 1 1 60E3E24F
P 7850 4750
F 0 "#PWR?" H 7850 4600 50  0001 C CNN
F 1 "+5V" V 7865 4878 50  0000 L CNN
F 2 "" H 7850 4750 50  0001 C CNN
F 3 "" H 7850 4750 50  0001 C CNN
	1    7850 4750
	0    -1   -1   0   
$EndComp
$Comp
L power:+5V #PWR?
U 1 1 60E3F0E6
P 7850 5150
F 0 "#PWR?" H 7850 5000 50  0001 C CNN
F 1 "+5V" V 7865 5278 50  0000 L CNN
F 2 "" H 7850 5150 50  0001 C CNN
F 3 "" H 7850 5150 50  0001 C CNN
	1    7850 5150
	0    -1   -1   0   
$EndComp
$Comp
L power:+5V #PWR?
U 1 1 60E402EF
P 8450 1850
F 0 "#PWR?" H 8450 1700 50  0001 C CNN
F 1 "+5V" V 8465 1978 50  0000 L CNN
F 2 "" H 8450 1850 50  0001 C CNN
F 3 "" H 8450 1850 50  0001 C CNN
	1    8450 1850
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR?
U 1 1 60E407A0
P 8450 4350
F 0 "#PWR?" H 8450 4200 50  0001 C CNN
F 1 "+5V" V 8465 4478 50  0000 L CNN
F 2 "" H 8450 4350 50  0001 C CNN
F 3 "" H 8450 4350 50  0001 C CNN
	1    8450 4350
	1    0    0    -1  
$EndComp
NoConn ~ 7850 2550
NoConn ~ 7850 5050
Text GLabel 7850 2050 0    50   Input ~ 0
Motor1_IN1
Text GLabel 7850 2150 0    50   Input ~ 0
Motor1_IN2
Text GLabel 7850 2450 0    50   Input ~ 0
Motor1_IN3
Text GLabel 7850 4550 0    50   Input ~ 0
Motor2_IN1
Text GLabel 7850 4650 0    50   Input ~ 0
Motor2_IN2
Text GLabel 7850 4950 0    50   Input ~ 0
Motor2_IN3
Text GLabel 6400 1500 2    50   Input ~ 0
Motor1_IN1
Text GLabel 6400 1600 2    50   Input ~ 0
Motor1_IN2
Text GLabel 6400 1700 2    50   Input ~ 0
Motor1_IN3
Text GLabel 6400 1800 2    50   Input ~ 0
Motor2_IN1
Text GLabel 6400 1900 2    50   Input ~ 0
Motor2_IN2
Text GLabel 6400 2000 2    50   Input ~ 0
Motor2_IN3
$Comp
L AS5600:AS5600 U?
U 1 1 60E47B96
P 9700 3200
F 0 "U?" H 9850 3637 60  0000 C CNN
F 1 "AS5600" H 9850 3531 60  0000 C CNN
F 2 "" H 9700 3200 60  0001 C CNN
F 3 "" H 9700 3200 60  0001 C CNN
	1    9700 3200
	1    0    0    -1  
$EndComp
NoConn ~ 9350 3150
$Comp
L power:+5V #PWR?
U 1 1 60E4F745
P 9350 3050
F 0 "#PWR?" H 9350 2900 50  0001 C CNN
F 1 "+5V" V 9365 3178 50  0000 L CNN
F 2 "" H 9350 3050 50  0001 C CNN
F 3 "" H 9350 3050 50  0001 C CNN
	1    9350 3050
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 60E503CA
P 9350 3350
F 0 "#PWR?" H 9350 3100 50  0001 C CNN
F 1 "GND" V 9355 3222 50  0000 R CNN
F 2 "" H 9350 3350 50  0001 C CNN
F 3 "" H 9350 3350 50  0001 C CNN
	1    9350 3350
	0    1    1    0   
$EndComp
Text GLabel 3800 2500 0    50   Input ~ 0
B_SDA
Text GLabel 3800 2600 0    50   Input ~ 0
B_SCL
Text GLabel 3800 2300 0    50   Input ~ 0
A_SDA
Text GLabel 3800 2400 0    50   Input ~ 0
A_SCL
Text GLabel 10350 3250 2    50   Input ~ 0
A_SDA
Text GLabel 10350 3150 2    50   Input ~ 0
A_SCL
$Comp
L AS5600:AS5600 U?
U 1 1 60E537FA
P 9700 5700
F 0 "U?" H 9850 6137 60  0000 C CNN
F 1 "AS5600" H 9850 6031 60  0000 C CNN
F 2 "" H 9700 5700 60  0001 C CNN
F 3 "" H 9700 5700 60  0001 C CNN
	1    9700 5700
	1    0    0    -1  
$EndComp
NoConn ~ 9350 5650
$Comp
L power:+5V #PWR?
U 1 1 60E53801
P 9350 5550
F 0 "#PWR?" H 9350 5400 50  0001 C CNN
F 1 "+5V" V 9365 5678 50  0000 L CNN
F 2 "" H 9350 5550 50  0001 C CNN
F 3 "" H 9350 5550 50  0001 C CNN
	1    9350 5550
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 60E53807
P 9350 5850
F 0 "#PWR?" H 9350 5600 50  0001 C CNN
F 1 "GND" V 9355 5722 50  0000 R CNN
F 2 "" H 9350 5850 50  0001 C CNN
F 3 "" H 9350 5850 50  0001 C CNN
	1    9350 5850
	0    1    1    0   
$EndComp
Text GLabel 10350 5750 2    50   Input ~ 0
B_SDA
Text GLabel 10350 5650 2    50   Input ~ 0
B_SCL
Wire Notes Line
	7300 1500 7300 3500
Wire Notes Line
	7300 3500 10800 3500
Wire Notes Line
	10800 3500 10800 1500
Wire Notes Line
	10800 1500 7300 1500
Wire Notes Line
	10800 6000 10800 4000
Wire Notes Line
	10800 4000 7300 4000
Wire Notes Line
	7300 4000 7300 6000
Wire Notes Line
	7300 6000 10800 6000
$Comp
L power:GND #PWR?
U 1 1 60E700E1
P 10350 3050
F 0 "#PWR?" H 10350 2800 50  0001 C CNN
F 1 "GND" V 10355 2922 50  0000 R CNN
F 2 "" H 10350 3050 50  0001 C CNN
F 3 "" H 10350 3050 50  0001 C CNN
	1    10350 3050
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 60E70BE3
P 10350 5550
F 0 "#PWR?" H 10350 5300 50  0001 C CNN
F 1 "GND" V 10355 5422 50  0000 R CNN
F 2 "" H 10350 5550 50  0001 C CNN
F 3 "" H 10350 5550 50  0001 C CNN
	1    10350 5550
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 60E7166B
P 10350 5850
F 0 "#PWR?" H 10350 5600 50  0001 C CNN
F 1 "GND" V 10355 5722 50  0000 R CNN
F 2 "" H 10350 5850 50  0001 C CNN
F 3 "" H 10350 5850 50  0001 C CNN
	1    10350 5850
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 60E71852
P 10350 3350
F 0 "#PWR?" H 10350 3100 50  0001 C CNN
F 1 "GND" V 10355 3222 50  0000 R CNN
F 2 "" H 10350 3350 50  0001 C CNN
F 3 "" H 10350 3350 50  0001 C CNN
	1    10350 3350
	0    -1   -1   0   
$EndComp
Text GLabel 9350 3250 0    50   Input ~ 0
A_OUT
Text GLabel 9350 5750 0    50   Input ~ 0
B_OUT
Text GLabel 6400 2200 2    50   Input ~ 0
A_OUT
Text GLabel 6400 2300 2    50   Input ~ 0
B_OUT
Text Notes 7350 1400 0    197  ~ 0
Motor_A
Text Notes 7350 3900 0    197  ~ 0
Motor_B
$EndSCHEMATC
