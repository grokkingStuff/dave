#+TITLE: Create The Future



* Sponsors

The Primary Sponsors of the 2021 Create the Future Design Contest (the “Contest”) are COMSOL, Inc. and Mouser Electronics. SAE Media Group, an SAE International Company, is the contest producer and owner.

* Our Entry

- Type of Entry
  - [ ] Individual
  - [X] Team
- Team Members
  - Alia Khan
  - Eman Rashid Ali
  - Danish Sheikh
  - Vi Kumar
- Entry Title

  Gravity-compensated Robotic Arm

- Category
  - [ ] Aerospace & Defense
  - [ ] Automotive/Transportation
  - [ ] Consumer Product Design
  - [ ] Electronics/Sensors/IoT
  - [X] Manufacturing/Robotics/Automation
  - [ ] Medical
  - [ ] Sustainable Technologies/Future Energy

** Description
:LOGBOOK:
CLOCK: [2021-07-05 Mon 12:51]--[2021-07-05 Mon 13:07] =>  0:16
CLOCK: [2021-06-05 Mon 16:09]--[2021-06-05 Mon 16:41] =>  0:32
CLOCK: [2021-07-05 Mon 00:09]--[2021-07-05 Mon 00:18] =>  0:09
:END:

Our goal is to reduce RSI and to automate tasks in industries that haven't used robotic arms yet due to high costs. We accomplish this by using gravity compensated robotic arms, allowing us to reduce motor cost, use simpler power electronics and control, and by designing backdriveable and inherently safe robotic arms.

Gravity compensation is currently utilized in prosthetics and surgical tools. Its use in robotic arms would enable lower cost motors and simpler algorithms to be utilized. The inherent backdriveability and safe behavior makes it suitable for collabrative work with humans.

Brushless gimbal motors are like normal brushless motors but wound for very low speed (ie lots of turns of thin wire) and many do indeed have more poles. They are best used in situation where their velocity is close to zero, but their torque is high and able to change rapidly, giving high precision in position control.

By shifting the gravity load from the motors to the springs, we can shift from using heavy-duty motors to the light and fast gimbal motors. In addition to the reduced cost from smaller and cheaper components, the energy expenditure is reduced from only having to move the inertial load.

Gravity-compensated robotic arms could be used in:
  - electronics manufacturing to replace humans in the placement of parts that cannot be loaded on reels.
  - assembly of palletized goods and packaging
  - movement of heavy loads without needing to segregate human and robot workplaces.

We are currently building a prototype and we've been making fast progress, thanks to the use of 3D printing, open source motor control, and drone motors.


** Illustration

- Illustration #1
  CAD
- Illustration #2
  Wiring Diagram
- Illustration #3
  Energy Cost without Gravity Balancer vs with gravity balancer

* Entry Requirements
** Time Stuff
The Contest opens for entries on March 1, 2021 and closes July 1, 2021 (referred to herein as the Contest Live Period). Entries must be received by 11:59 pm EDT on July 1, 2021. For your entries to qualify for consideration, you must meet the following requirements:

** Entry Category

Choose one of seven categories for your entry:
- [ ] Aerospace & Defense
  Product innovations with applications in the aerospace, aviation, and/or defense markets.

- [ ] Automotive/Transportation
  Products that enable movement of people and goods from one place to another.

- [ ] Consumer Product Design
  Products that increase quality of life in the workplace, at home, or while traveling.

- [ ] Electronics/Sensors/IoT
  Products that improve computing, communications, sensing, test, and other fields that rely on advances in electronic components, boards and systems; products that enable an interconnected world – the Internet of Things (IoT) and Industrial Internet of Things (IIoT).

- [X] Manufacturing/Robotics/Automation
  Products that speed, improve, and/or automate work, manufacturing, and research & development (R&D).

- [ ] Medical
  Products that improve the efficiency/quality of healthcare and save or improve lives.

- [ ] Sustainable Technologies/Future Energy
  “Green” products that reduce dependence on non-renewable energy resources, as well as products designed for other purposes using environmentally friendly materials or manufacturing processes.

** Technical Abstract

Provide a description of your entry (up to 500 words) in the form of a technical abstract, in English. Your description should cover how the entry works, what makes it novel, how it would be produced, and where it would be applied. Entries will be judged on these criteria:

*** Innovation
(50% of score)

  /*How is the design novel?*/

  Uses springs to remove the inertial load on motors. Helps shift from stepper motors and complicated BLDC control mechanisms.

  /*Does it represent an important advance over the current state of the art?*/

  Gravity compensation is currently utilized in prosthetics. Its use in robotic arms would enable lower cost motors and simpler algorithms to be utilized. The inherent backdriveability and safe behavior makes it suitable for collabrative work with humans.

*** Feasibility/Manufacturability
  (25% of score)

  - /*How easy would it be to implement?*/

    The robotic arm components required allow for fast protoyping and quick development. Given existing designs for camera gimbals and field-oriented control of BLDC,


  - /*Could it be cost-effectively manufactured/produced?*/

    Has it been demonstrated to work?
    (Please note: prototypes are NOT required.)

    The robotic arm is designed using low-cost manufacturing techniques and easy to procure parts.
    The rise in drone and camera gimbal markets have led to significant cost reductions for brushless gimbal motors as well as the availability of SBCs and cheap microcontrollers.
    We are currently building a prototype.

*** Marketability
  (25%)
  Does the idea have practical applications?
  Is there a well-defined, significant market for its use?

  Would be effective in current applications of robotic arms (automobile, pharmaceuticals, and machining).
  Gravity-compensated robotic arms could be used in:
  - electronics manufacturing to replace humans in the placement of parts that cannot be loaded on reels.
  - assembly of palletized goods and packaging
  - movement of heavy loads without needing to segregate human and robot workplaces.

** Visual Illustrations
Upload at least one (but no more than three) visual illustrations. File type options:
        PDF(single-page), GIF, JPEG, PNG. The visual illustration(s) should complement your entry description and can be drawings, photos, charts, or CAD images. File size should be kept under 500kb per illustration.

Entries submitted without an illustration automatically will be disqualified, no exceptions. We cannot accept PowerPoint presentations, white papers, or other documents as illustrations. All descriptive text should be entered in the space provided on the Entry Form. Text on illustrations should be limited to captions or call outs.

** Video
You also have the option of submitting video to support your entry (limited to one video per entry). Maximum length: three minutes. First, upload your video to YouTube or Vimeo. Then, when you complete the Entry Form, provide the link to the video where indicated on the form. We cannot accept any video files by e-mail. Video is optional.

** Prototypes
It is NOT required to have a prototype of your product. However, if a working prototype does exist, please indicate this where asked on the Entry Form. Entrants are encouraged to include an illustration of their prototype with the entry materials.

** Support
If you are having difficulty submitting your entry, please contact us.


* Judging

To select the Grand Prize and Category winners, and to determine the Top 100 entries overall, SAE Media Group will recruit a minimum of 24 judges from its editorial board of readers.
The judging panel also will be open to one representative from each Primary Sponsor. The judges are prohibited from entering the contest themselves or evaluating entries from any organization at which they are employed.
All decisions of the judges will be final.



** TIPS FOR ENTRANTS

The best entries clearly and concisely answer the following questions and are accompanied by an illustration that complements and illuminates the text:

- What problem does your design idea solve?
- What are the potential benefits?
- How does your design work?
- How is your idea novel or an improvement on what is currently available in the marketplace?
- Where would this idea be applied?
- What is the market potential?
- How would your product be manufactured?
- How would the production cost compare with products already in the marketplace?


The best design ideas will serve a public good by:

- Improving quality of life
- Preventing or reducing injuries
- Saving lives
- Improving public safety and security
- Saving time and money
- Improving productivity
- Automating tedious tasks
- Offering alternative energy solutions
- Reducing consumption of natural resources
- Reducing waste
- Creating jobs
- Bolstering the economy
- Enabling other product improvements

** Top 3 Reasons Entries are disqualified from the contest

- No illustration provided.
  An illustration must accompany your entry and provide a visual representation of your idea.

- Entry description is too short.
  While you do not need to submit the max 500 words, your description should be of sufficient length to describe how the invention works, what makes it novel, how it would be produced, and where it would be applied. If it is only a few sentences, chances are it’s too short. We suggest you review last year’s winning entries to get an idea of length and what you should cover.

- Illustrations submitted in an unacceptable format and/or with an excessive amount of text.
  As spelled out in the Rules, acceptable file types include PDF (single-page), GIF, JPEG, or PNG. If you attempt to submit a Word or PowerPoint document, your entry will not be accepted. And as noted above, text on illustrations should be limited to captions or call-outs.
