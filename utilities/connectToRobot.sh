########################################################
# Displays a list of all flags with their descriptions
# Globals:
#   None
# Arguments:
#   None
# Returns:
#   None
########################################################
usage() {
    echo "$0 usage:" &&              \           
      grep "[[:space:]].)\ ##" $0 |  \         # Find all line in script that have '##' after a ')' 
      sed 's/##//' |                 \         # Replace all '##' with nothing
      sed -r 's/([a-z])\)/-\1/';              # TODO Can't remember
}
#################################################################
# Detects the operating system that this script is being run on
# Globals:
#   OSTYPE
# Arguments:
#   None
# Returns:
#   MACHINE
#################################################################
function detect_operating_system() {

    local MACHINE
    MACHINE=""      
    
    case "$OSTYPE" in

    #########################################################################
    # *nix systems                                                          #
    #########################################################################
        solaris*) 
            MACHINE="SOLARIS"                                                     # Do people even use Solaris anymore? Gosh, haven't heard this name in a while.
            ;;
        darwin*) 
            MACHINE="OSX"
            ;;
        linux*)
            MACHINE="LINUX"
            ;;
        bsd*)
            MACHINE="BSD"
            ;;    
    #    aix*)
    #        MACHINE="AIX"
    #        ;;    
    #    #Was gonna add AIX but I dunno if it has the $OSTYPE variable and I don't really care.
    

    #########################################################################
    # windows systems                                                       #
    #########################################################################
        cygwin*)
            MACHINE="WINDOWS"
            ;&                                                                    # Since Windows has two options for $OSTYPE, we're gonna let it cascade into the next case
        msys*)
            MACHINE="WINDOWS"

                                                                                  # We're using uname -s to figure out which shell in Windows we're using.
            unameOut = "$(uname -s)"
            case "${unameOut}" in
                CYGWIN*)
                    MACHINE="WINDOWS-CYGWIN"
                    # This should work for git shell as well. 
                    # I'm not sure why you're using git-shell to do anything except run git commands but cool. You do you, mate.
                    ;;
                MINGW32_NT*)
                    MACHINE="WINDOWS-32"
                    ;;
                MINGW64_NT*)
                    MACHINE="WINDOWS-64"
                    ;;
                Linux*)
                    MACHINE="WINDOWS-POWERSHELL"
                    # Not sure why Powershell returns Linux when uname-s is passed to it. Seems janky.
                    echo "This script will not run in Powershell. Please install a bash shell."
                    echo "Terminating program."
                    exit 1

            esac
            ;;
    
    #########################################################################
    # This shouldn't happen but I'm super interested if it does!            #
    #########################################################################
        *)
            MACHINE="unknown: $OSTYPE"
            echo "I don't know what you're running but I'm interested! Send me an email at grokkingStuff@gmail.com"
            echo "I mean, seriously, what are you running! Is it a really old system and if so, can you send me pics? pretty please!"
            echo "I'm guessing you're running some sort of custom unix machine so as long as you have access to bash, you should be good."
            echo "If you do have issues, do send me a email but I can't promise I can make it work on your system."
            ;;
    esac

    # Time to return the answer
    return $MACHINE
}
## Description: Connects to remote server and relays local changes made in git repo and opens a shell in remote server.
## Written by: Vishakh Kumar - grokkingStuff@gmail.com on 04-2018

## Dependencies: shpass

while getopts ":iulh:" o; do
    case "${o}" in

        i) ## IP Address flag. Specify ip address. Default is 143.215.98.17
            REMOTE_IPADDRESS="${OPTARG}" 
            readonly REMOTE_IPADDRESS
            ;;

        u) ## Remote username flag. Specify username of raspberry pi. Default is 'pi'
            REMOTE_USER="${OPTARG}" 
            readonly REMOTE_USER
            ;;

        f) ## Location of remote folder flag. Specify location of github repo on raspberry pi. Change only if not working on 2018 folder 
            REMOTE_LOCATION="${OPTARG}"
            readonly REMOTE_LOCATION 
            ;;

        p) ## Password flag. Specify a password for user on remote server
            REMOTE_USER_PASSWORD="${OPTARG}"
            readonly REMOTE_USER_PASSWORD
            ;;

        h) ## Help flag. Displays flag options 
            usage
            exit 0
            ;;

        :)  # For when a mandatory argument is skipped.
            err "Option -$OPTARG requires an argument."
            exit 1
            ;;
        *) 
            usage
            err "Unexpected option ${flag}"
            exit 1 
            ;;
    esac
done
###########################################################
# Allows for user to send time-tagged strings into STDERR
# Globals:
#   None
# Arguments:
#   Array of String(s)
# Returns:
#   None
###########################################################
function err() {
  echo "[$(date +'%Y-%m-%dT%H:%M:%S%z')]: $@" >&2
}
